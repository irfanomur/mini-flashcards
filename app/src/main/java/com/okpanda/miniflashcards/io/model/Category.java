
package com.okpanda.miniflashcards.io.model;

import com.google.gson.annotations.SerializedName;

/**
 * This represent a category that is used for filtering words.
 */
public class Category {
    @SerializedName("name")
    private String mName;

    public Category(String name) {
        mName = name;
    }

    public String getName() {
        return mName;
    }
}

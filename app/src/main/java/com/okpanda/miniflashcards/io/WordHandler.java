
package com.okpanda.miniflashcards.io;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;

import com.okpanda.miniflashcards.io.model.Word;

import java.util.ArrayList;
import java.util.Collections;

public class WordHandler {

    private ArrayList<Word> mWords = new ArrayList<>();

    public WordHandler(String dataBody)  {
        JsonArray jsonArray = new JsonParser().parse(dataBody).getAsJsonObject().get("words").getAsJsonArray();
        Word[] wordArray = new Gson().fromJson(jsonArray, Word[].class);
        Collections.addAll(mWords, wordArray);
    }

    public ArrayList<Word> getWords() {
        return mWords;
    }
}

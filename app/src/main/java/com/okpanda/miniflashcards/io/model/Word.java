/*
 * Copyright (c) 2016 Irfan Omur.
 */

package com.okpanda.miniflashcards.io.model;

import com.google.gson.annotations.SerializedName;

/**
 * This represent a word.
 */
public class Word {
    @SerializedName("en")
    private String mEn;

    @SerializedName("ja")
    private String mJa;

    @SerializedName("sound")
    private String mSound;

    @SerializedName("explanation")
    private String mExplanation;

    @SerializedName("usage")
    private String mUsage;

    @SerializedName("category")
    private String[] mCategory;

    public Word(String en, String ja, String sound, String explanation, String usage, String[] category) {
        mEn = en;
        mJa = ja;
        mSound = sound;
        mExplanation = explanation;
        mUsage = usage;
        mCategory = category;
    }

    public String getEn() {
        return mEn;
    }

    public String getJa() {
        return mJa;
    }

    public String getSound() {
        return mSound;
    }

    public String getExplanation() {
        return mExplanation;
    }

    public String getUsage() {
        return mUsage;
    }

    public String[] getCategories() {
        return mCategory;
    }
}

package com.okpanda.miniflashcards.io;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;

import com.okpanda.miniflashcards.io.model.Category;

import java.util.ArrayList;
import java.util.Collections;

public class CategoryHandler {

    private ArrayList<Category> mCategories = new ArrayList<>();

    public CategoryHandler(String dataBody) {
        JsonArray jsonArray = new JsonParser().parse(dataBody).getAsJsonObject().get("categories").getAsJsonArray();
        Category[] categoryArray = new Gson().fromJson(jsonArray, Category[].class);
        Collections.addAll(mCategories, categoryArray);
    }

    public ArrayList<Category> getCategories() {
        return mCategories;
    }
}

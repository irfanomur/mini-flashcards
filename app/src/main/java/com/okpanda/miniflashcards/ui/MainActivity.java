package com.okpanda.miniflashcards.ui;

import com.okpanda.miniflashcards.R;
import com.okpanda.miniflashcards.io.CategoryHandler;
import com.okpanda.miniflashcards.io.WordHandler;
import com.okpanda.miniflashcards.io.model.Category;
import com.okpanda.miniflashcards.io.model.Word;
import com.okpanda.miniflashcards.provider.Data;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private List<Category> mCategories = new ArrayList<>();
    private ArrayList<Word> mWords = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Load data from local storage
        loadData();

        SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        ViewPager viewPager = (ViewPager) findViewById(R.id.container);
        viewPager.setAdapter(sectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

    }

    private void loadData() {
        try {
            String json = Data.loadData(this);
            if (json != null) {
                mCategories = new CategoryHandler(json).getCategories();
                mWords = new WordHandler(json).getWords();
            }
        } catch (IOException | NullPointerException e) {
            // TODO: Show an error message
        }
    }

    public ArrayList<Word> getWords() {
        return mWords;
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    private class SectionsPagerAdapter extends FragmentPagerAdapter {

        SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a CategoryFragment.

            final String category = position == 0 ? CategoryFragment.CATEGORY_ALL :
                    mCategories.get(position - 1).getName();

            return CategoryFragment.newInstance(category);
        }

        @Override
        public int getCount() {
            return (mCategories == null ? 0 : mCategories.size()) + 1;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return position == 0 ? CategoryFragment.CATEGORY_ALL :
                    mCategories.get(position-1).getName();
        }
    }
}

package com.okpanda.miniflashcards.ui;

import com.okpanda.miniflashcards.R;
import com.okpanda.miniflashcards.io.model.Word;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * A placeholder fragment containing a simple view.
 */
public class CategoryFragment extends Fragment {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_CATEGORY = "category";

    public static final String CATEGORY_ALL = "all";

    public CategoryFragment() {
    }

    /**
     * Returns a new instance of this fragment for the given category.
     */
    public static CategoryFragment newInstance(String category) {
        CategoryFragment fragment = new CategoryFragment();
        Bundle args = new Bundle();
        args.putString(ARG_CATEGORY, category);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);

        RecyclerView list = (RecyclerView) rootView.findViewById(R.id.words_list);
        final Context context = list.getContext();
        list.addItemDecoration(new DividerItemDecoration(context, DividerItemDecoration.VERTICAL));
        list.setLayoutManager(new LinearLayoutManager(context));

        list.setAdapter(new WordAdapter(context));

        return rootView;
    }

    // TODO: Activity shouldn't be reached from Fragment. To get data from activity, Presenter architecture is better.
    private ArrayList<Word> getWords() {
        Activity parentActivity = getActivity();
        if (parentActivity != null && parentActivity instanceof MainActivity) {
            ArrayList<Word> wordsFromActivity = ((MainActivity) parentActivity).getWords();
            String category = getArguments().getString(ARG_CATEGORY);

            if (category != null && category.equals(CATEGORY_ALL)) {
                return wordsFromActivity;
            }

            // Filter data for each category
            ArrayList<Word> filteredWords = new ArrayList<>();
            for (Word word : wordsFromActivity) {
                for (String item : word.getCategories()) {
                    if (item.equals(category)) {
                        filteredWords.add(word);
                    }
                }
            }

            return filteredWords;
        }
        return null;
    }

    /**
     * Adapter that displays a list of words.
     * This includes its english word, japanese translation, a button for sound sound.
     */
    private class WordAdapter extends RecyclerView.Adapter<ItemHolder> {

        private final Context mContext;
        private ArrayList<Word> mWords = new ArrayList<>();

        WordAdapter(Context context) {
            mContext = context;
            mWords = getWords();
        }

        @Override
        public ItemHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
            final ItemHolder holder = new ItemHolder(LayoutInflater.from(mContext), parent);
            holder.itemView.setOnClickListener(mOnClickListener);
            return holder;
        }

        @Override
        public void onBindViewHolder(final ItemHolder holder, final int position) {
            final String word = mWords.get(position).getEn();
            final String translation = mWords.get(position).getJa();

            holder.word.setText(word);
            holder.translation.setText(translation);

            holder.soundButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        MediaPlayer player = new MediaPlayer();
                        player.setAudioStreamType(AudioManager.STREAM_MUSIC);
                        player.setDataSource(mWords.get(position).getSound());
                        player.prepare();
                        player.start();
                    } catch (Exception e) {
                        // TODO: handle exception
                    }
                }
            });

        }



        private View.OnClickListener mOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                final ViewGroup.LayoutParams lp = v.getLayoutParams();
                if (!(lp instanceof RecyclerView.LayoutParams)) {
                    return;
                }
                final int position = ((RecyclerView.LayoutParams) lp).getViewAdapterPosition();
                if (position == RecyclerView.NO_POSITION) {
                    return;
                }
                final Word word = mWords.get(position);
                // TODO: Start detail activity

                Intent intent =new Intent(getActivity(), DetailActivity.class);
                intent.putExtra(DetailActivity.ARG_WORD, word.getEn());
                intent.putExtra(DetailActivity.ARG_EXPLANATION, word.getExplanation());
                intent.putExtra(DetailActivity.ARG_USAGE, word.getUsage());
                startActivity(intent);
            }

        };

        @Override
        public int getItemCount() {
            return mWords.size();
        }
    }

    private static class ItemHolder extends RecyclerView.ViewHolder {

        TextView word;
        TextView translation;
        ImageView soundButton;

        ItemHolder(LayoutInflater inflater, ViewGroup parent) {
            super(inflater.inflate(R.layout.word_item, parent, false));
            word = (TextView) itemView.findViewById(R.id.word);
            translation = (TextView) itemView.findViewById(R.id.translation);
            soundButton = (ImageView) itemView.findViewById(R.id.sound_button);
        }

    }

}
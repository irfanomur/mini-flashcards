package com.okpanda.miniflashcards.ui;

import com.okpanda.miniflashcards.R;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

public class DetailActivity extends AppCompatActivity {

    public static final String ARG_WORD = "word";
    public static final String ARG_EXPLANATION = "explanation";
    public static final String ARG_USAGE = "usage";

    // RATING 0: User has not rated yet.
    // RATING 1: don't know
    // RATING 2: kind of
    // RATING 3: good
    // RATING 4: very easy
    private static final int RATE_NO_RATE = 0;
    private static final int RATE_DONT_KNOW = 1;
    private static final int RATE_KIND_OF = 2;
    private static final int RATE_GOOD = 3;
    private static final int RATE_VERY_EASY = 4;

    private SharedPreferences sp;

    private String word;
    private View mDontKnowButton;
    private View mKindOfButton;
    private View mGoodButton;
    private View mVeryEasyButton;
    private View mCurrentRatedButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            word = extras.getString(ARG_WORD);
            String explanation = extras.getString(ARG_EXPLANATION);
            String usage = extras.getString(ARG_USAGE);
            if (explanation != null) {
                ((TextView) findViewById(R.id.explanation)).setText(explanation);
            }
            if (usage != null) {
                ((TextView) findViewById(R.id.usage)).setText(usage);
            }
            if (word != null) {
                setTitle(word);
            } else {
                // TODO: show error
                finish();
            }
        }

        findViewById(R.id.back_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        sp = PreferenceManager.getDefaultSharedPreferences(this);

        mDontKnowButton = findViewById(R.id.dont_know);
        mKindOfButton = findViewById(R.id.kind_of);
        mGoodButton = findViewById(R.id.good);
        mVeryEasyButton = findViewById(R.id.very_easy);

        mDontKnowButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRate(RATE_DONT_KNOW);
                setCurrentRateView();
            }
        });
        mKindOfButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRate(RATE_KIND_OF);
                setCurrentRateView();
            }
        });
        mGoodButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRate(RATE_GOOD);
                setCurrentRateView();
            }
        });
        mVeryEasyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRate(RATE_VERY_EASY);
                setCurrentRateView();
            }
        });

        setCurrentRateView();
    }

    private void setCurrentRateView() {
        int currentRate = getRate();

        // Firstly, uncheck rated button, if there is one
        if (mCurrentRatedButton != null) {
            mCurrentRatedButton.setBackgroundColor(Color.WHITE);
        }
        switch (currentRate) {
            case RATE_DONT_KNOW:
                mCurrentRatedButton = mDontKnowButton;
                mDontKnowButton.setBackgroundColor(Color.BLUE);
                break;
            case RATE_KIND_OF:
                mCurrentRatedButton = mKindOfButton;
                mKindOfButton.setBackgroundColor(Color.BLUE);
                break;
            case RATE_GOOD:
                mCurrentRatedButton = mGoodButton;
                mGoodButton.setBackgroundColor(Color.BLUE);
                break;
            case RATE_VERY_EASY:
                mCurrentRatedButton = mVeryEasyButton;
                mVeryEasyButton.setBackgroundColor(Color.BLUE);
                break;
        }
    }

    private void setRate(int rate) {
        sp.edit().putInt(word, rate).apply();
    }

    private int getRate() {
        return sp.getInt(word, RATE_NO_RATE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
